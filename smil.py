# !/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):

    def __init__(self):
        self.MiLista = []
        self.attrs = {'root-layout': ['width', 'height', 'background-color'],
                      'region': ['id', 'top', 'bottom', 'left', 'right'],
                      'img': ['src', 'region', 'begin', 'end', 'dur'],
                      'audio': ['src', 'begin', 'dur'],
                      'textstream': ['src', 'region', 'fill']}

    def startElement(self, name, attrs):
        dicc = {}
        if name in self.attrs:
            dicc = {'attrs': [], 'name': name}
            for atributos in self.attrs[name]:
                valoratributos = attrs.get(atributos, "")
                if valoratributos != "":
                    dicc["attrs"].append((atributos, valoratributos))
            self.MiLista.append(dicc)

    def get_tags(self):
        return self.MiLista


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))


if __name__ == "__main__":
    main()

