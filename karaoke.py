# !/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smil import SMILHandler
import sys


def to_string(MiLista):
    texto = ""
    for elemento in MiLista:
        etiquetas = elemento['name']
        atributos = elemento['attrs']
        texto = texto + etiquetas
        for atributo in atributos:
            nombre = atributo[0]
            valor = atributo[1]
            texto = f"{texto}\t{nombre}={valor}\t"
            texto += "\n"

    return texto


def main():
    """Programa principal"""
    if len(sys.argv) == 2:
        try:
            fichero = sys.argv[1]
        except IndexError:
            sys.exit("Usage: python3 karaoke.py <file>")

        parser = make_parser()
        cHandler = SMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(fichero))
        MiLista = cHandler.get_tags()
        print(to_string(MiLista))


if __name__ == "__main__":
    main()

