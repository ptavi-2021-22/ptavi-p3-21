# !/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """Clase para manejar chistes malos"""

    def __init__(self):

        """Constructor. Inicializamos las variables"""

        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False
        self.count = 0

    def startElement(self, name, attrs):

        """Método que se llama cuando se abre una etiqueta"""

        if name == 'chiste':
            # De esta manera tomamos los valores de los atributos
            self.count = self.count + 1
            self.calificacion = attrs.get('calificacion', "")
            print(f"Chiste número: {self.count} ({self.calificacion})")
        elif name == 'pregunta':
            self.inPregunta = True
        elif name == 'respuesta':
            self.inRespuesta = True

    def endElement(self, name):

        """Método que se llama al cerrar una etiqueta"""

        if name == 'pregunta':
            self.inPregunta = False
        if name == 'respuesta':
            self.inRespuesta = False

    def characters(self, char):

        """Método para tomar contenido de la etiqueta"""

        if self.inPregunta:
            self.pregunta = self.pregunta + char
        elif self.inRespuesta:
            self.respuesta += char

        if self.pregunta != "" and self.respuesta != "" and char == "\n":
            print(f"Pregunta: {self.pregunta}")
            print(f"Respuesta: {self.respuesta}")
            print()
            self.pregunta = ""
            self.respuesta = ""


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = ChistesHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('chistes.xml'))


if __name__ == "__main__":
    main()



